#include "stdafx.h"
#include "Client.h"
#include "StratKamikaze.h"

using namespace std;


bool StratKamikaze::Process(CLIENT& c, std::map<int, JOB>& jobs) const
{
	int count = 0;
	MAP_OBJECT* last;
	for (auto & u : c.mParser.Units) {
		if (u.side != 0) {
			continue;
		}

		++count;
		last = &u;
	}
	if (count != 8)
		return false;

	POS eHatch = POS(33, 33);

	if (last->pos != eHatch)
	{
		c.mUnitTarget[last->id].c = CLIENT::CMD_MOVE;
		c.mUnitTarget[last->id].pos = eHatch;
		jobs[last->id].hasJob = true;
	}
	else
	{
		c.mUnitTarget[last->id].c = CLIENT::CMD_ATTACK;
		c.mUnitTarget[last->id].target_id = c.mParser.EnemyHatchery.id;
	}

	return false;
}


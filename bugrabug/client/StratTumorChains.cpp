#include "stdafx.h"
#include "StratTumorChains.h"

#include <set>

#include "Client.h"
#include "parser.h"

StratTumorChains::StratTumorChains(CLIENT & client)
	: client(client)
	, parser(client.mParser)
{
}

bool StratTumorChains::Process(CLIENT & client, std::map<int, JOB>&) const
{
	if (parser.tick < 83)
		return false;

	std::vector<MAP_OBJECT> const & Tumors = client.mParser.CreepTumors;
	
	std::set<POS> tumor_positions;
	for (auto tumor : parser.CreepTumors)
		tumor_positions.insert(tumor.pos);

	for (auto tumor : Tumors) if (tumor.energy >= 60 && tumor.side == 0)
	{
		std::vector<POS> creepable;
		creepableCells(creepable, tumor);
		std::vector<POS> candidates;
		spreadCandidates(candidates, creepable);
		std::vector<POS> best;
		bestChoice(best, candidates);

		std::vector<POS> const & toSpread = best.empty() ? candidates : best;

		int dist = 0;
		POS wat(-1, -1);
		for (auto cell : toSpread)
		{
			int const x = std::abs(tumor.pos.x - cell.x);
			int const y = std::abs(tumor.pos.y - cell.y);
			int const sd = x*x + y*y;
			if (sd > dist && tumor_positions.find(cell) == tumor_positions.end())
			{
				dist = sd;
				wat.x = cell.x;
				wat.y = cell.y;
			}
		}

		if (wat.x > -1)
			client.command_buffer << "creep_tumor_spawn " << tumor.id << " " << wat.x << " " << wat.y << std::endl;
	}

	return false;
}

bool StratTumorChains::validPos(POS const & p) const
{
	return 0 <= p.x && p.x < 40 && 0 <= p.y && p.y < 40;
}

// p0 poz�ci�j� cella k�zepe k�r� rajzolt radius sugar� k�r�n bel�li cell�k
void StratTumorChains::validCellsInARadius(std::vector<POS> & cells, POS const & p0, int radius) const
{
	for (int dy = -radius + 1; dy < radius; ++dy)
		for (int dx = -radius + 1; dx < radius; ++dx)
		{
			POS p(p0.x + dx, p0.y + dy);
			if (!validPos(p))
				continue;
			int dx_q1 = 2 * dx + (0 < dx ? 1 : -1);
			int dy_q1 = 2 * dy + (0 < dy ? 1 : -1);
			int d2_q2 = dx_q1*dx_q1 + dy_q1*dy_q1;
			if (d2_q2 <= radius*radius * 4)
				cells.push_back(p);
		}
}

// ha ez a cella szabad �s nincs rajta creep,
// a szomsz�dban valahol van creep, akkor ide terjeszkedhet
// el�felt�tel hogy a cella egy gener�tor ter�let�n bel�l legyen
bool StratTumorChains::spreadCandidate(POS const & p) const
{
	return PARSER::CREEP == parser.GetAt(p);
}

// adott poziciok kozul megmondja melyekbe mehet creep
bool StratTumorChains::spreadCandidates(std::vector<POS> & candidates, std::vector<POS> const & cells) const
{
	for (auto p : cells)
		if (spreadCandidate(p))
			candidates.push_back(p);
	return !candidates.empty();
}

// berakja a vektorba a terjeszkedesi sugaron belul levo cellakat
void StratTumorChains::creepableCells(std::vector<POS> & cells, MAP_OBJECT const & o) const
{
	validCellsInARadius(cells, o.pos, 10);
}

void StratTumorChains::bestChoice(std::vector<POS> & cells, std::vector<POS> const & in) const
{
	for (auto p : in)
		if ((p.x > 0 && p.y > 0 && parser.GetAt(POS(p.x - 1, p.y - 1)) != PARSER::CREEP && parser.GetAt(POS(p.x - 1, p.y - 1)) != PARSER::WALL) ||
			(p.y > 0 && parser.GetAt(POS(p.x, p.y - 1)) != PARSER::CREEP && parser.GetAt(POS(p.x, p.y - 1)) != PARSER::WALL) ||
			(p.y > 0 && p.x < 39 && parser.GetAt(POS(p.x + 1, p.y - 1)) != PARSER::CREEP && parser.GetAt(POS(p.x + 1, p.y - 1)) != PARSER::WALL) ||
			(p.x > 0 && parser.GetAt(POS(p.x - 1, p.y)) != PARSER::CREEP && parser.GetAt(POS(p.x - 1, p.y)) != PARSER::WALL) ||
			(parser.GetAt(POS(p.x, p.y)) != PARSER::CREEP && parser.GetAt(POS(p.x, p.y)) != PARSER::WALL) ||
			(p.x < 39 && parser.GetAt(POS(p.x + 1, p.y)) != PARSER::CREEP && parser.GetAt(POS(p.x + 1, p.y)) != PARSER::WALL) ||
			(p.x > 0 && p.y < 39 && parser.GetAt(POS(p.x - 1, p.y + 1)) != PARSER::CREEP && parser.GetAt(POS(p.x - 1, p.y + 1)) != PARSER::WALL) ||
			(p.y < 39 && parser.GetAt(POS(p.x, p.y + 1)) != PARSER::CREEP && parser.GetAt(POS(p.x, p.y + 1)) != PARSER::WALL) ||
			(p.y < 39 && p.x < 39 && parser.GetAt(POS(p.x + 1, p.y + 1)) != PARSER::CREEP && parser.GetAt(POS(p.x + 1, p.y + 1)) != PARSER::WALL))
		{
			cells.push_back(p);
		}
}

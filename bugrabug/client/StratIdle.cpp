#include "stdafx.h"
#include "Client.h"
#include "StratIdle.h"
#include <algorithm>
#include <iterator>
#include <set>

using namespace std;

static const int kozepsav=15;

std::vector<POS> PreferredQueenPosAtBorder(CLIENT& c)
{

	std::vector<MAP_OBJECT> farthest_tumors;
	std::copy_if(c.mParser.CreepTumors.begin(), c.mParser.CreepTumors.end(), std::back_inserter(farthest_tumors), [](const MAP_OBJECT& tumor) {return tumor.side == 0 ? true : false; });
	std::sort(farthest_tumors.begin(), farthest_tumors.end(), [&c](const MAP_OBJECT& lhtumor, const MAP_OBJECT& rhtumor)
	{
		return c.mDistCache.GetDist(lhtumor.pos, c.mParser.OwnHatchery.pos) < c.mDistCache.GetDist(rhtumor.pos, c.mParser.OwnHatchery.pos);
	});

//	const int dist_of_creep_of_farthest_tumor = c.mDistCache.GetDist(farthest_tumors.back().pos, POS(0, 0)) + 15;
	std::vector<POS> sajat_creep_szele;
	std::vector<POS> enemy_creep_szele;
	PARSER::eGroundType szomszedok[4];
	for (int x = 38; x >=1; --x)
	{
		for (int y=38; y >= 1; --y)
		{
//			if (x + y < 15 || x + y>65) continue;
//			if (x + y > dist_of_creep_of_farthest_tumor) continue;
			if (c.mParser.GetAt(POS(x, y)) == PARSER::CREEP)
			{
				szomszedok[0] = c.mParser.GetAt(POS(x + 1, y));
				szomszedok[1] = c.mParser.GetAt(POS(x - 1, y));
				szomszedok[2] = c.mParser.GetAt(POS(x, y + 1));
				szomszedok[3] = c.mParser.GetAt(POS(x, y - 1));
				const bool hatar = std::any_of(std::begin(szomszedok), std::end(szomszedok), [](const PARSER::eGroundType& gt) {return (gt != PARSER::CREEP && gt != PARSER::WALL);});
				if (hatar)
					sajat_creep_szele.push_back(POS(x, y));
			}
/*			else if (c.mParser.GetAt(POS(x, y)) == PARSER::ENEMY_CREEP)
			{
				szomszedok[0] = c.mParser.GetAt(POS(x + 1, y));
				szomszedok[1] = c.mParser.GetAt(POS(x - 1, y));
				szomszedok[2] = c.mParser.GetAt(POS(x, y + 1));
				szomszedok[3] = c.mParser.GetAt(POS(x, y - 1));
				const bool hatar = std::any_of(std::begin(szomszedok), std::end(szomszedok), [](const PARSER::eGroundType& gt) {return gt == PARSER::CREEP || gt == PARSER::EMPTY; });
				if (hatar)
					enemy_creep_szele.emplace_back(POS(x, y));
			}*/
			else
				continue;
		}
	}

	auto legtavolabb_az_enemytol = [](const POS& lhs, const POS& rhs) {return lhs.x + lhs.y < rhs.x + rhs.y; };

	// kis x-ek, bal oldal
	std::vector<POS> posvec;
	POS bal_legjobb, fent_legjobb, kozepen_legjobb;
	for (const POS& pos : sajat_creep_szele)
	{
		if (pos.x < 10)
			posvec.push_back(pos); // osszegyujtjuk a bal oldali cuccokat
	}
	if(posvec.size()!=0)
		bal_legjobb = *std::max_element(posvec.begin(), posvec.end(), legtavolabb_az_enemytol);

	// kis y-ek, teteje
	for (const POS& pos : sajat_creep_szele)
	{
		if (pos.y < 10)
			posvec.push_back(pos);
	}
	if(posvec.size()!=0)
		fent_legjobb = *std::max_element(posvec.begin(), posvec.end(), legtavolabb_az_enemytol);

	// kozepen levo reszek
	for (const POS& pos : sajat_creep_szele)
	{
		if (std::abs(pos.x - pos.y) < kozepsav)
			posvec.push_back(pos); // osszegyujtjuk a bal oldali cuccokat
	}
	if(posvec.size()!=0)
		kozepen_legjobb = *std::max_element(posvec.begin(), posvec.end(), legtavolabb_az_enemytol);

	std::vector<POS> result;
//	if(bal_legjobb!=POS())
		result.push_back(bal_legjobb);
//	if(kozepen_legjobb!=POS())
		result.push_back(kozepen_legjobb);
//	if(fent_legjobb!=POS())
		result.push_back(fent_legjobb);
//	if(result.size()==0)
//		result.push_back(POS(10,10));
	return result;
}



bool StratIdle::Process(CLIENT& c, std::map<int, JOB>& jobs) const
{
	auto positions = PreferredQueenPosAtBorder(c);

	std::set<POS> tumor_positions;
	for (auto tumor : c.mParser.CreepTumors)
		tumor_positions.insert(tumor.pos);

	int num_of_queens_in_center=0;
	int bal_id=-1, fent_id=-1;
	std::vector<int> kozep_id;
	std::map<int,int> melyikpozihozlegkozelebb; // id, pozi idx
	for(MAP_OBJECT& queen : c.mParser.Units) if(queen.side==0)
	{
		std::vector<int> dist;
		dist.resize(3);
		std::fill(dist.begin(),dist.end(),1000);

		if(positions[0]!=POS())
			dist[0] = c.mDistCache.GetDist(queen.pos,positions[0]);
		if(positions[1]!=POS())
			dist[1] = c.mDistCache.GetDist(queen.pos,positions[1]);
		if(positions[2]!=POS())
			dist[2] = c.mDistCache.GetDist(queen.pos,positions[2]);
		auto it = std::min_element(dist.begin(), dist.end());
		if(it!=dist.end())
		{
			melyikpozihozlegkozelebb[queen.id] = std::distance(dist.begin(),it);
		}
	}
	int bal=0, fent=0;
	for(auto queen : melyikpozihozlegkozelebb)
	{
		if(queen.second==0)
		{
			if(bal==0)
				bal=1;
			else
				queen.second=1;
		}
		if(queen.second==2)
		{
			if(fent==0)
				fent=1;
			else
				queen.second=1;
		}
	}


	int count = 0;
	for (auto & u : c.mParser.Units) {
		if (u.side != 0 || jobs[u.id].hasJob) {
			continue;
		}

		// ha tudok, szarok
		if (u.energy > 100)
		{
			bool ok = false;
			for (int dx = -1; dx < 2 && !ok; ++dx)
			{
				for (int dy = -1; dy < 2; ++dy)
				{
					int cx = u.pos.x + dx;
					int cy = u.pos.y + dy;
					POS p(cx, cy);
					if (cx >= 0 && cx < 40 && cy > 0 && cy < 40)
					{
						if (c.mParser.GetAt(p) == PARSER::CREEP && u.pos.IsNear(p) && tumor_positions.find(p) == tumor_positions.end())
						{
							c.mUnitTarget[u.id].c = CLIENT::CMD_SPAWN;
							c.mUnitTarget[u.id].pos = p;
							ok = true;
							break;
						}
					}
				}
			}
			if (ok)
				continue;
		}

//		POS closest(39,39);
//		int minDist = 1000;
//		for (POS p : positions) {
//			int d = c.mDistCache.GetDist(p, u.pos);
//			if (c.mDistCache.GetDist(p, u.pos) < minDist) {
//				minDist = d;
//				closest = p;
//			}
//		}
		if(count==0)
		{
			if(positions[0]!=POS())
			{
				c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
				c.mUnitTarget[u.id].pos = positions[0];
			}
			else
			{
				c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
				c.mUnitTarget[u.id].pos = positions[1];
			}
		}
		if(count==1)
		{
			if(positions[2]!=POS())
			{
				c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
				c.mUnitTarget[u.id].pos = positions[2];
			}
			else
			{
				c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
				c.mUnitTarget[u.id].pos = positions[1];
			}
		}
		if(count >1)
		{
				c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
				c.mUnitTarget[u.id].pos = positions[1];

		}
/*
		if(melyikpozihozlegkozelebb.find(u.id)!=melyikpozihozlegkozelebb.end())
		{
			c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
			c.mUnitTarget[u.id].pos = positions[melyikpozihozlegkozelebb[u.id]];
		}
		else
		{
			c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
			c.mUnitTarget[u.id].pos = positions[1];
		}*/
/*		int szam = %3;
		for(; positions[szam]==POS(); szam = rand()%3);
		c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
		c.mUnitTarget[u.id].pos = positions[szam];*/
		count++;
	}

	return false;
}


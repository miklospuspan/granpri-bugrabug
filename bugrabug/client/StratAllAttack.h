#pragma once

#include "Strategy.h"

class CLIENT;

class StratAllAttack : public Strategy
{
public:
	virtual bool Process(CLIENT&, std::map<int, JOB>&) const;
};
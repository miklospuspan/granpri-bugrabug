#pragma once

#include "Strategy.h"

class CLIENT;

class StratFlee: public Strategy
{
public:
	StratFlee(CLIENT& ac) : c(ac) {}
	virtual bool Process(CLIENT&, std::map<int, JOB>&) const;
private:
	void GetNearQueens(const POS& pos, std::vector<MAP_OBJECT*>& FriendlyQueens, std::vector<MAP_OBJECT*>& EnemyQueens) const;
	CLIENT& c;
};
#include "stdafx.h"
#include "Client.h"
#include "parser.h"
#include <memory>

#include "StratBeginningSpread.h"

class MYCLIENT : public CLIENT
{
public:
	MYCLIENT();
protected:
	virtual std::string GetPassword() { return std::string("Yp7jfc"); }
	virtual std::string GetPreferredOpponents() { return std::string("test"); }
	virtual bool NeedDebugLog() { return true; }
	virtual void Process();
	std::vector<POS> PreferredQueenPosAtBorder();
	virtual void reset();
private:
	std::vector<std::unique_ptr<Strategy>> strategies;
};


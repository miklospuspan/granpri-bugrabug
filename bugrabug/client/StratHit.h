#pragma once

#include "Strategy.h"

class CLIENT;

class StratHit: public Strategy
{
public:
	virtual bool Process(CLIENT&, std::map<int, JOB>&) const;
};
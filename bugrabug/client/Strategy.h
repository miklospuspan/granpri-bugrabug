#pragma once

#include "Client.h"
class CLIENT;

struct Strategy
{
	virtual bool Process(CLIENT &, std::map<int, JOB>&) const = 0;
};

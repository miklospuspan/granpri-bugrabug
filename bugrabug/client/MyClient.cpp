#include "stdafx.h"
#include "Client.h"
#include "MyClient.h"
#include "parser.h"
#include <memory>

#include "StratAllAttack.h"
#include "StratBeginningSpread.h"
#include "StratTumorChains.h"
#include "StratIdle.h"
#include "StratHit.h"
#include "StratKamikaze.h"

MYCLIENT::MYCLIENT()
{
	strategies.emplace_back(new StratBeginningSpread());
	strategies.emplace_back(new StratTumorChains(*this));
	strategies.emplace_back(new StratHit());
	strategies.emplace_back(new StratAllAttack());
	strategies.emplace_back(new StratKamikaze());
	strategies.emplace_back(new StratIdle());
}

void MYCLIENT::Process()
{
	for (auto & u : mParser.Units) {
		u.hp2 = u.hp;
	}

	std::map<int, JOB> jobs;

	for (auto const & s : strategies)
	{
		if (s->Process(*this, jobs))
			break;
	}
}

void MYCLIENT::reset()
{
	strategies.clear();
	strategies.emplace_back(new StratBeginningSpread());
	strategies.emplace_back(new StratTumorChains(*this));
	strategies.emplace_back(new StratHit());
	strategies.emplace_back(new StratAllAttack());
	strategies.emplace_back(new StratIdle());
}

CLIENT *CreateClient()
{
	return new MYCLIENT();
}

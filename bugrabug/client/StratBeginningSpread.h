#pragma once

#include "Strategy.h"
#include "Client.h"

struct StratBeginningSpread : public Strategy
{
	virtual bool Process(CLIENT &, std::map<int, JOB>&) const;
};

#include "stdafx.h"
#include "Client.h"
#include "StratAllAttack.h"

using namespace std;

int DistToHatchery(POS p, POS hatchP, CLIENT& c)
{
	int res = 200;
	for (int x = 0; x < HATCHERY_SIZE; ++x)
		for (int y = 0; y < HATCHERY_SIZE; ++y)
			res = min(res, c.mDistCache.GetDist(p, hatchP.ShiftXY(x, y)));

	return res;
}

bool StratAllAttack::Process(CLIENT& c, std::map<int, JOB>& jobs) const
{
	POS fHatch = POS(4, 4);
	POS eHatch = POS(33, 33);



	//int defense = 0;
	//int attack = 0;

	//vector<int> distToHatchery(c.mParser.Units.size());

	const int CALC_TILL = 60;
	const int hit_points = 4;
	vector<vector<int>> offenseAtTick(CALC_TILL);
	vector<int> defenseAtTick(CALC_TILL);

	for (auto & u : c.mParser.Units) {
		if (u.side == 0) {
			int d = DistToHatchery(u.pos, eHatch, c);
			if (d * 2 < u.hp)
				offenseAtTick[d].push_back(u.hp - d * 2);
		}
		else {
			int d = DistToHatchery(u.pos, eHatch, c);
			++defenseAtTick[d];
		}
	}

	int hatcheryHits = max(0, c.mParser.EnemyHatchery.hp - 1) / 40;
	for (int i = 1; i < CALC_TILL; i++)
	{
		for (int hp : offenseAtTick[i - 1])
			if(hp >= 0)
				offenseAtTick[i].push_back(hp);

		hatcheryHits -= offenseAtTick[i].size();
		if (hatcheryHits <= 0)
			break;

		defenseAtTick[i] += defenseAtTick[i - 1];
		int defenders = defenseAtTick[i];

		for (int& hp : offenseAtTick[i]) {
			if (!defenders)
				break;

			while (hp > 0 && defenders) {
				defenders--;
				hp -= 40;
			}
		}

		for (int& hp : offenseAtTick[i]) {
			hp -= 2;
		}

	}

	int my = 0;
	int their = 0;
	for (int x = 1; x < 40; ++x)
	{
		for (int y = 1; y < 40; ++y)
		{
			if (c.mParser.GetAt(POS(x, y)) == PARSER::CREEP)
				my++;

			if (c.mParser.GetAt(POS(x, y)) == PARSER::ENEMY_CREEP)
				their++;
					
		}
	}

	if (hatcheryHits <= 0 || (c.mParser.tick > 1000 && their>my) || (their>2*my) ) {
		for (auto & u : c.mParser.Units) if (u.side == 0)
		{
			if (u.pos == eHatch)
			{
				c.mUnitTarget[u.id].c = CLIENT::CMD_ATTACK;
				c.mUnitTarget[u.id].target_id = c.mParser.EnemyHatchery.id;
			}
			else
			{
				c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
				c.mUnitTarget[u.id].pos = eHatch;
			}
		}
		return true;
	}
	

	return false;
}


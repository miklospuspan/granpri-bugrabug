#pragma once

#include "Strategy.h"

class CLIENT;
struct MAP_OBJECT;
struct POS;
class PARSER;
class CLIENT;

struct StratTumorChains : public Strategy
{
	StratTumorChains(CLIENT &);

	virtual bool Process(CLIENT &, std::map<int, JOB>&) const;

	// thx bela!
	bool validPos(POS const & p) const;
	void validCellsInARadius(std::vector<POS> & cells, POS const &p0, int radius) const;
	bool spreadCandidate(POS const &p) const;
	bool spreadCandidates(std::vector<POS> &candidates, std::vector<POS> const &cells) const;
	void creepableCells(std::vector<POS> &cells, MAP_OBJECT const &o) const;
	void bestChoice(std::vector<POS> &, std::vector<POS> const &) const;

private:
	CLIENT & client;
	PARSER & parser;
};

#include "stdafx.h"
#include "StratBeginningSpread.h"
#include <sstream>
#include <algorithm>

bool StratBeginningSpread::Process(CLIENT & client, std::map<int, JOB>&) const
{
	switch (client.mParser.tick)
	{
	case 1:
		for (auto &queen : client.mParser.Units) if (queen.side == 0)
		{
			client.mUnitTarget[queen.id].c = CLIENT::CMD_SPAWN;
			client.mUnitTarget[queen.id].pos = POS(7, 14);
		}
		break;
	case 8:
		for (auto &queen : client.mParser.Units) if (queen.side == 0)
		{
			client.mUnitTarget[queen.id].c = CLIENT::CMD_SPAWN;
			client.mUnitTarget[queen.id].pos = POS(14, 10);
		}
		break;
	case 68:
	{
		std::vector<int> available_ids;
		for (auto & tumor : client.mParser.CreepTumors) if (tumor.side == 0 && tumor.energy == 60)
			available_ids.push_back(tumor.id);
		if (!available_ids.empty())
		{
			std::sort(available_ids.begin(), available_ids.end());
			client.command_buffer << "creep_tumor_spawn " << available_ids[0] << " 13 20" << std::endl;
		}
		break;
	}
	case 82:
	{
		std::vector<int> available_ids;
		for (auto & tumor : client.mParser.CreepTumors) if (tumor.side == 0 && tumor.energy == 60)
			available_ids.push_back(tumor.id);
		if (!available_ids.empty())
		{
			std::sort(available_ids.begin(), available_ids.end());
			client.command_buffer << "creep_tumor_spawn " << available_ids[0] << " 21 5" << std::endl;
		}
		break;
	}
	default: break;
	};

	return client.mParser.tick < 25;
}

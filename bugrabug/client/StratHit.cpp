#include "stdafx.h"
#include "Client.h"
#include "StratHit.h"
#include <algorithm>
#include <iterator>

using namespace std;

vector<MAP_OBJECT*> GetHittableQueens(CLIENT& c, POS p)
{
	vector<MAP_OBJECT*> res;
	for (auto & u : c.mParser.Units) {
		if (u.side != 1) {
			continue;
		}

		if (u.pos == p.ShiftDir(POS::SHIFT_UP)
			|| u.pos == p.ShiftDir(POS::SHIFT_DOWN)
			|| u.pos == p.ShiftDir(POS::SHIFT_RIGHT)
			|| u.pos == p.ShiftDir(POS::SHIFT_LEFT)
			|| u.pos == p)
		{
			res.push_back(&u);
		}
	}

	return res;
}

vector<MAP_OBJECT*> GetHittableTumors(CLIENT& c, POS p)
{
	vector<MAP_OBJECT*> res;
	for (auto & u : c.mParser.CreepTumors) {
		if (u.side != 1) {
			continue;
		}

		if (u.pos == p.ShiftDir(POS::SHIFT_UP)
			|| u.pos == p.ShiftDir(POS::SHIFT_DOWN)
			|| u.pos == p.ShiftDir(POS::SHIFT_RIGHT)
			|| u.pos == p.ShiftDir(POS::SHIFT_LEFT)
			|| u.pos == p)
		{
			res.push_back(&u);
		}
	}

	return res;
}

bool SelectTarget(vector<MAP_OBJECT*> targets, CLIENT& c, std::map<int, JOB>& jobs, int uid)
{
	if (targets.empty())
		return false;

	int target;
	int targetHp = 10000;
	for (auto q : targets)
	{
		if (q->hp2 <= 0)
			continue;
		if ((targetHp > 40 && q->hp2 < targetHp) || (q->hp2 <= 40 && q->hp2 > targetHp))
		{
			target = q->id;
			targetHp = q->hp2;
			q->hp2 = max(0, q->hp2-40);
		}
	}
	if (targetHp != 10000)
	{
		c.mUnitTarget[uid].c = CLIENT::CMD_ATTACK;
		c.mUnitTarget[uid].target_id = target;
		jobs[uid].hasJob = true;
	}
	return true;
}


bool StratHit::Process(CLIENT& c, std::map<int, JOB>& jobs) const
{
	for (auto & u : c.mParser.Units) {
		if (u.side != 0) {
			continue;
		}

		vector<MAP_OBJECT*> queens = GetHittableQueens(c, u.pos);
		if (SelectTarget(queens,c, jobs, u.id))
			continue;


		vector<MAP_OBJECT*> tumors = GetHittableTumors(c, u.pos);
		if (SelectTarget(tumors, c, jobs, u.id))
			continue;
	}

	return false;
}


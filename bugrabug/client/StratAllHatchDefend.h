#pragma once

#include "Strategy.h"

class CLIENT;

class StratAllHatchDefend : public Strategy
{
public:
	virtual bool Process(CLIENT&, std::map<int, JOB>&) const;
};
#include "stdafx.h"
#include "Client.h"
#include "StratFlee.h"
#include <algorithm>
#include <iterator>

using namespace std;

//

bool StratFlee::Process(CLIENT& c, std::map<int, JOB>& jobs) const
{
	/*for (auto & u : c.mParser.Units) {
		if (u.side != 0) {
			continue;
		}

		vector<MAP_OBJECT*> queens = GetHittableQueens(c, u.pos);
		if (queens.empty())
			continue;

		int target;
		int targetHp = 10000;
		for (auto q : queens)
		{
			if ((targetHp > 40 && q->hp < targetHp) || (q->hp <=40 && q->hp > targetHp))
			{
				target = q->id;
				targetHp = q->hp;
			}
		}
		if(targetHp == 10000)
			continue; // shouzldnet happen

		c.mUnitTarget[u.id].c = CLIENT::CMD_ATTACK;
		c.mUnitTarget[u.id].target_id = target;
		jobs[u.id].hasJob = true;
	}
	*/
	return false;
}

void StratFlee::GetNearQueens(const POS& pos, std::vector<MAP_OBJECT*>& FriendlyQueens, std::vector<MAP_OBJECT*>& EnemyQueens) const
{
	FriendlyQueens.clear();
	EnemyQueens.clear();
	for(MAP_OBJECT& queen : c.mParser.Units)
	{
		if(queen.side==0) 
			FriendlyQueens.push_back(&queen);
		else if (queen.side==1)
			EnemyQueens.push_back(&queen);
	}
	std::sort(FriendlyQueens.begin(), FriendlyQueens.end(), [this,&pos](const MAP_OBJECT* lhqueen, const MAP_OBJECT* rhqueen)
	{
		return c.mDistCache.GetDist(lhqueen->pos, pos) < c.mDistCache.GetDist(rhqueen->pos, pos);
	});
	std::sort(EnemyQueens.begin(), EnemyQueens.end(), [this,&pos](const MAP_OBJECT* lhqueen, const MAP_OBJECT* rhqueen)
	{
		return c.mDistCache.GetDist(lhqueen->pos, pos) < c.mDistCache.GetDist(rhqueen->pos, pos);
	});
}

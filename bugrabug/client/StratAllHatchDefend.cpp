#include "stdafx.h"
#include "Client.h"
#include "StratAllHatchDefend.h"

using namespace std;
/*
int DistToHatchery2(POS p, POS hatchP, CLIENT& c)
{
	int res = 200;
	for (int x = 0; x < HATCHERY_SIZE; ++x)
		for (int y = 0; y < HATCHERY_SIZE; ++y)
			res = min(res, c.mDistCache.GetDist(p, hatchP.ShiftXY(x, y)));

	return res;
}
*/
bool StratAllHatchDefend::Process(CLIENT& c, std::map<int, JOB>& jobs) const
{/*
	//POS fHatch = POS(4, 4);
	POS eHatch = POS(4, 4);

	static bool found = false;
	if (found)
	{
		for (auto & u : c.mParser.Units) if (u.side == 0)
		{
			if (u.pos != eHatch)
				continue;
			c.mUnitTarget[u.id].c = CLIENT::CMD_ATTACK;
			c.mUnitTarget[u.id].target_id = c.mParser.EnemyHatchery.id;
		}
		return true;
	}

	//int defense = 0;
	//int attack = 0;

	//vector<int> distToHatchery(c.mParser.Units.size());

	const int CALC_TILL = 60;
	const int hit_points = 4;
	vector<int> offenseAtTick(CALC_TILL);
	vector<int> defenseAtTick(CALC_TILL);

	for (auto & u : c.mParser.Units) {
		if (u.side == 1) {
			int d = DistToHatchery2(u.pos, eHatch, c);
			if (d * 2 < u.hp)
				++offenseAtTick[d];
		}
		else {
			int d = DistToHatchery2(u.pos, eHatch, c);
			++defenseAtTick[d];
		}
	}
	/*
	int hatcheryHits = 38;
	for (int i = 1; i < CALC_TILL; i++)
	{
		for (int hp : offenseAtTick[i - 1])
			if(hp >= 0)
				offenseAtTick[i].push_back(hp);

		hatcheryHits -= offenseAtTick[i].size();
		if (hatcheryHits <= 0)
			break;

		defenseAtTick[i] += defenseAtTick[i - 1];
		int defenders = defenseAtTick[i];

		for (int& hp : offenseAtTick[i]) {
			if (!defenders)
				break;

			while (hp > 0 && defenders) {
				defenders--;
				hp -= 40;
			}
		}

		for (int& hp : offenseAtTick[i]) {
			hp -= 2;
		}

	}*/

/*	if (hatcheryHits <= 0) {
		for (auto & u : c.mParser.Units) if (u.side == 0)
		{
			//c.mUnitTarget[u.id].c = CLIENT::CMD_ATTACK;
			c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
			c.mUnitTarget[u.id].pos = eHatch;
		}
		found = true;
		return true;
	}
	
	for (auto & u : c.mParser.Units) if (u.side == 0)
	{
		if (jobs[u.id].hasJob)
			continue;
		c.mUnitTarget[u.id].c = CLIENT::CMD_MOVE;
		c.mUnitTarget[u.id].pos = POS(27, 27);
	}*/

	return false;
}

